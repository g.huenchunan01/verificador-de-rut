package com.example.Verificador_rut;
import java.util.Scanner;
public class VerificadorRut {
    private String rut;

    public VerificadorRut(String rut){
        this.setRut(rut);
    }

    public String getRut(){
        return rut;
    }

    public void setRut(String rut) {
        if(this.validarRut(rut)){
            System.out.println("Rut guardado");
            this.rut=rut;
        }else{
            System.out.println("Error rut invalido");
            System.out.println("Ingrese un rut valido");
            Scanner leerRut=new Scanner(System.in);
            String nuevoRut= leerRut.nextLine();
            if(this.validarRut(nuevoRut)){
                System.out.println("Rut guardado");
                this.rut=nuevoRut;
            }else {
                this.setRut(rut);
            }
        }
    }
    public void mostrar(VerificadorRut v){
        System.out.println("rut: "+v.getRut());
    }

    public boolean esNumero(String string){
        try{
            Integer.parseInt(string);
            return  true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean validarRut(String rut){
        try {
            String numeros=rut.substring(0,8);
            char guion=rut.charAt(8);
            char dv=rut.charAt(9);
            boolean bguion=false;
            boolean brut=false;
            boolean bdv=false;

            if(guion=='-'){
            bguion=true;
            }else{
                System.out.println("Error falta guion o se encuentra separado");
            }
            if(this.esNumero(numeros)&&rut.length()==10){
                brut=true;
            }else{
                System.out.println("Error 'El formato incorrecto'");
            }
            if(dv=='0'||dv=='1'||dv=='2'||dv=='3'||dv=='4'||dv=='5'||dv=='6'||dv=='7'||dv=='8'||dv=='9'||dv=='k'){
                bdv=true;
            }else{
                System.out.println("Error: DV no valido");
            }
            boolean retorno = bguion && brut && bdv;
            return retorno;
        }catch (Exception e){
            System.out.println("Error");
            return  false;
        }
    }
}
